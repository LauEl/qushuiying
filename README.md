# 抖音去水印

#### 介绍
抖音一键去水印插件，基于PHP Curl.

#### 软件架构
程序直接使用apache或者nginx运行即可
php版本需7.0+
程序无需数据库

#### 须知
只能放在根目录运行不得二级目录
如果需要放在二级目录执行修改代码中“/static/”为：./static/ 因为二级目录资源找不到或者下载二级目录文件把文件名改为index.html就可以

如果需要使用php5.6的需要改语法：
api.php中的70行的代码中，有php7新特性的语法 "data??[]"php7以下的版本，修改"data??[]"为"data" ，就能解决了


```
/**
 * 输出错误JSON
@param string 错误信息
@param int 错误代码
@return json
 */
function  jerr ($msg='error',$code=500,$data=false){
	header("content:application/json;chartset=uft-8");
	echo json_encode (["code"=>$code,"msg"=>$msg,"data"=>$data]);
	die ;
}
```



#### 在线体验
[http://wdway.com/dy/](http://wdway.com/dy/)


![输入图片说明](image.png)

#### 关于上传到二级目录空白的问题

将二级
index.html(二级目录)的（二级目录）删除
删除原来的index.html 就不空白了。